﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Interna
{
    public class InternaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Interna";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Interna_default",
                "Interna/{controller}/{action}/{id}",
                new { action = "Index", controller = "IntHome", id = UrlParameter.Optional }
            );
        }
    }
}