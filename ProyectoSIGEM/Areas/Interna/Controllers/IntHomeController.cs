﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Interna.Controllers
{
    //[Authorize(Roles = "Admin,Empleado")]
    public class IntHomeController : Controller
    {
        // GET: Interna/IntHome
        public ActionResult Index()
        {
            return View();
        }
    }
}