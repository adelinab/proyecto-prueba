﻿using AutoMapper;
using ProyectoSIGEM.Servicios.Matriculas;
using ProyectoSIGEM.Servicios.Matriculas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoSIGEM.Areas.Interna.Models;
using ProyectoSIGEM.Servicios.Excepciones;
using ProyectoSIGEM.Models;
using ProyectoSIGEM.Servicios.Escuelas.Dtos;
using ProyectoSIGEM.Servicios.Escuelas;
using ProyectoSIGEM.Servicios.Alumnos;

namespace ProyectoSIGEM.Areas.Interna.Controllers
{
    [Authorize(Roles = "Empleado,Admin")]
    public class MatriculasController : Controller
    {
        private ServicioMatriculas servicioMatriculas;
        private ServicioEscuelas servicioEscuelas;
        private ServicioAlumnos servicioAlumnos;
        private ViewModelBuilder builder;

        public MatriculasController() {

            builder = new ViewModelBuilder();
            servicioEscuelas = new ServicioEscuelas();
            servicioMatriculas = new ServicioMatriculas();
            servicioAlumnos = new ServicioAlumnos();
        }

        // GET: Interna/Matriculas
        public ActionResult Index(string busqueda)
        {
            busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
            var model = Mapper.Map<IList<MatriculaDto>, IList<MatriculasListaViewModel>>(servicioMatriculas.Buscar(busqueda));
            
            return View(new BuscaMatriculasViewModel() { Matriculas = model });
        }

        public ActionResult Nueva()
        {
            //TODO: traer data Real
            MatriculaViewModel matricula = builder.MatriculaViewModel();
            var escuelas = Mapper.Map<IList<EscuelasDto>, IList<EscuelasListaViewModel>>(servicioEscuelas.Buscar(""));
            matricula.Escuelas = new SelectList(escuelas, "EscuelaId", "Nombre", matricula.EscuelaId);
            return View(matricula);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nueva(MatriculaGrabarViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar la reserva
                    MatriculaDto matriculaDto =
                        Mapper.Map<MatriculaGrabarViewModel, MatriculaDto>(model);
                    //servicioMatriculas.Reservar(matriculaDto);
                    if (!matriculaDto.FecIngreso.CompararFechas(matriculaDto.FecReporte))
                    {
                        ModelState.AddModelError("", "La fecha de ingreso no puede ser mayor a la fecha de reporte");
                        return View(builder.MatriculaViewModel(model));
                    }
                    int alumnoid = servicioAlumnos.TraerPorDNI(model.AlumnoId.ToString());
                    matriculaDto.AlumnoId = alumnoid;
                    servicioMatriculas.Nueva(matriculaDto);
                    return RedirectToAction("Index", "Matriculas", new { area = "Interna" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(builder.MatriculaViewModel(model));
            }
            catch (ErrorEnMatricula ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(builder.MatriculaViewModel(model));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PartialViewResult ConsultarMatriculas(string busqueda)
        {
            busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
            var model = Mapper.Map<IList<MatriculaDto>, IList<MatriculasListaViewModel>>(
                servicioMatriculas.Buscar(busqueda));

            return PartialView("_ListaMatriculas", model);
        }

        public ActionResult Editar(int id)
        {
            MatriculaDto matricula = servicioMatriculas.TraerPorId(id);
            if (matricula.EsNulo())
                RedirectToAction("NoEncontrado", "Errores", new { area = "" });
            matricula.AlumnoId = int.Parse(servicioAlumnos.TraerPorId(matricula.AlumnoId).DocumentoIdentidad);
            var escuelas = Mapper.Map<IList<EscuelasDto>, IList<EscuelasListaViewModel>>(servicioEscuelas.Buscar(""));
            MatriculaViewModel matriculaV = Mapper.Map<MatriculaDto, MatriculaViewModel>(matricula);

            matriculaV.Escuelas = new SelectList(escuelas, "EscuelaId", "Nombre", matricula.EscuelaId);

           // matricula.EscuelaId = int.Parse(servicioEscuelas.TraerPorId(matricula.EscuelaId).Nombre);
            return View(matriculaV);
        }

        [HttpPost]
        public ActionResult Editar(MatriculaGrabarViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var matricula = Mapper.Map<MatriculaGrabarViewModel, MatriculaDto>(model);
                    matricula.AlumnoId = servicioAlumnos.TraerPorDNI(matricula.AlumnoId.ToString());
                    servicioMatriculas.Actualizar(matricula);
                    
                    return RedirectToAction("Index", "Matriculas", new { area = "Interna" });
                }
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(builder.MatriculaViewModel(model));
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(model);
            }
        }


        public ActionResult Eliminar(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    servicioMatriculas.Eliminar(id);
                    return RedirectToAction("Index", "Matriculas", new { area = "Interna" });
                }
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return RedirectToAction("Index", "Matriculas", new { area = "Interna" });
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return RedirectToAction("Index", "Matriculas", new { area = "Interna" });
            }
        }

    }

}