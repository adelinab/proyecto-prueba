﻿using ProyectoSIGEM.Servicios.Escuelas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoSIGEM.Models;
using ProyectoSIGEM.Areas.Interna.Models;
using AutoMapper;
using ProyectoSIGEM.Servicios.Escuelas.Dtos;
using ProyectoSIGEM.Servicios.Excepciones;

namespace ProyectoSIGEM.Areas.Interna.Controllers
{
    [Authorize(Roles = "Empleado,Admin")]
    public class EscuelasController : Controller
    {
        private ServicioEscuelas servicioEscuelas;

        public EscuelasController()
        {
            servicioEscuelas = new ServicioEscuelas();
        }
        // GET: Interna/Escuelas
        public ActionResult Index(string busqueda)
        {
            busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
            var model = Mapper.Map<IList<EscuelasDto>, IList<EscuelasListaViewModel>>(servicioEscuelas.Buscar(busqueda));
            return View(new BuscaEscuelasViewModel() { Escuelas = model });
        }

        public ActionResult Nuevo()
        {
            return View(new GrabaEscuelaViewModel());
        }

        [HttpPost]
        public ActionResult Nuevo(GrabaEscuelaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar la reserva
                    servicioEscuelas.Nuevo(Mapper.Map<GrabaEscuelaViewModel, GrabaEscuelaDto>(model));
                    // transferencia de datos entre capas
                    return RedirectToAction("Index", "Escuelas", new { area = "Interna" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(model);
            }
            catch (ErrorCreandoEscuela ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Editar(int id)
        {
            EscuelasDto escuela = servicioEscuelas.TraerPorId(id);
            if (escuela.EsNulo())
                RedirectToAction("NoEncontrado", "Errores", new { area = "" });

            return View(Mapper.Map<EscuelasDto, GrabaEscuelaViewModel>(escuela));
        }

        [HttpPost]
        public ActionResult Editar(GrabaEscuelaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var escuela = Mapper.Map<GrabaEscuelaViewModel, EscuelasDto>(model);
                    servicioEscuelas.Actualizar(escuela);
                    return RedirectToAction("Index", "Escuelas", new { area = "Interna" });
                }
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(model);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(model);
            }
        }


        public ActionResult Eliminar(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    servicioEscuelas.Eliminar(id);
                    return RedirectToAction("Index", "Escuelas", new { area = "Interna" });
                }
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return RedirectToAction("Index", "Escuelas", new { area = "Interna" });
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return RedirectToAction("Index", "Escuelas", new { area = "Interna" });
            }
        }

        //public JsonResult ListaSimple(string busqueda)
        //{
        //    busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
        //    var alumnos = Mapper
        //        .Map<IList<EscuelasDto>, IList<EscuelasListaViewModel>>(
        //            servicioEscuelas.Buscar(busqueda)
        //        );

        //    return Json(alumnos, JsonRequestBehavior.AllowGet);
        //}

        public PartialViewResult ConsultarEscuelas(string busqueda)
        {
            busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
            var model = Mapper.Map<IList<EscuelasDto>, IList<EscuelasListaViewModel>>(
                servicioEscuelas.Buscar(busqueda));

            return PartialView("_ListaEscuelas", model);
        }
    }
}