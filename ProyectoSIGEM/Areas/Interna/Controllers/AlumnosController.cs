﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoSIGEM.Servicios.Alumnos;
using ProyectoSIGEM.Areas.Interna.Models;
using AutoMapper;
using ProyectoSIGEM.Servicios.Alumnos.Dtos;
using ProyectoSIGEM.Servicios.Excepciones;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Areas.Interna.Controllers
{
    [Authorize(Roles = "Empleado,Admin")]
    public class AlumnosController : Controller
    {
        private ServicioAlumnos servicioAlumnos;

        public AlumnosController() {
            servicioAlumnos = new ServicioAlumnos();
        }
        // GET: Interna/Clientes
        public ActionResult Index(string busqueda)
        {
            busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
            var model = Mapper.Map<IList<AlumnosDto>, IList<AlumnosListaViewModel>>(servicioAlumnos.Buscar(busqueda));
            return View(new BuscaAlumnosViewModel() { Alumnos = model });
        }

        public ActionResult Nuevo()
        {
            return View(new GrabaAlumnoViewModel());
        }

        [HttpPost]
        public ActionResult Nuevo(GrabaAlumnoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar la reserva
                    servicioAlumnos.Nuevo(Mapper.Map<GrabaAlumnoViewModel, GrabaAlumnoDto>(model));
                    // transferencia de datos entre capas
                    return RedirectToAction("Index", "Alumnos", new { area = "Interna" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(model);
            }
            catch (ErrorCreandoAlumno ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Editar(int id)
        {
            AlumnosDto alumno = servicioAlumnos.TraerPorId(id);
            if (alumno.EsNulo())
                RedirectToAction("NoEncontrado", "Errores", new { area = "" });

            return View(Mapper.Map<AlumnosDto, GrabaAlumnoViewModel>(alumno));
        }

        [HttpPost]
        public ActionResult Editar(GrabaAlumnoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var alumno = Mapper.Map<GrabaAlumnoViewModel, AlumnosDto>(model);
                    servicioAlumnos.Actualizar(alumno);
                    return RedirectToAction("Index", "Alumnos", new { area = "Interna" });
                }
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(model);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(model);
            }
        }

        
        public ActionResult Eliminar(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    servicioAlumnos.Eliminar(id);
                    return RedirectToAction("Index", "Alumnos", new { area = "Interna" });
                }
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return RedirectToAction("Index", "Alumnos", new { area = "Interna" });
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return RedirectToAction("Index", "Alumnos", new { area = "Interna" });
            }
        }

        //public JsonResult ListaSimple(string busqueda)
        //{
        //    busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
        //    var alumnos = Mapper
        //        .Map<IList<AlumnosDto>, IList<AlumnosListaViewModel>>(
        //            servicioAlumnos.Buscar(busqueda)
        //        );

        //    return Json(alumnos, JsonRequestBehavior.AllowGet);
        //}

        public PartialViewResult ConsultarAlumnos(string busqueda)
        {
            busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
            var model = Mapper.Map<IList<AlumnosDto>, IList<AlumnosListaViewModel>>(
                servicioAlumnos.Buscar(busqueda));

            return PartialView("_ListaAlumnos", model);
        }

    }
}