﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Areas.Interna.Models
{
    public class EscuelasListaViewModel
    {
        public int EscuelaId { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
    }
}
