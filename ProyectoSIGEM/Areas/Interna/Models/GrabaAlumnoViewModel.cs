﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Interna.Models
{
    public class GrabaAlumnoViewModel
    {
        public int AlumnoId { get; set; }
        [Required]
        [Display(Name = "Nombre:")]
        [MaxLength(60)]
        public string Nombre { get; set; }


        [Required]
        [Display(Name = "DNI:")]
        [MaxLength(8)]
        [Digits(ErrorMessage = "Ingrese solo dígitos")]
        public string DocumentoIdentidad { get; set; }


        public string DireccionDepartamento { get; set; }
        [Required]
        [Display(Name = "Departamento:")]
        public SelectList Departamentos { get; set; }


        public string DireccionProvincia { get; set; }
        [Required]
        [Display(Name = "Provincia:")]        
        public SelectList Provincias { get; set; }


        public string DireccionDistrito { get; set; }
        [Required]
        [Display(Name = "Distrito:")]
        public SelectList Distritos { get; set; }


        [Required]
        [Display(Name = "Dirección:")]
        [MaxLength(60)]
        public string DireccionCalle { get; set; }


        [Required]
        [Display(Name = "Teléfono:")]
        [MaxLength(12)]
        public string Telefono { get; set; }


        public GrabaAlumnoViewModel()
        {
            var emptyList = new List<string> { "Seleccione..." };
            Departamentos = new SelectList(emptyList);
            Provincias = new SelectList(emptyList);
            Distritos = new SelectList(emptyList);
        }
    }
}