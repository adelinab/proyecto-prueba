﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Interna.Models
{
   public class MatriculaGrabarViewModel
        {
            public int AlumnoId { get; set; }
            public int EscuelaId { get; set; }
            public string EscuelaEmisora { get; set; }
            public string Curso { get; set; }
            public string Categoria { get; set; }
            public decimal CostoTotal { get; set; }
            public string Promotor { get; set; }
            public DateTime FecIngreso { get; set; }
            public DateTime FecReporte { get; set; }
            [AllowHtml]
            public string Observacion { get; set; }
        } 
        public class MatriculaViewModel
        {
            public MatriculaViewModel()
            {
                FecIngreso = DateTime.Now;
                FecReporte = DateTime.Now;
            }

            public string AlumnoId { get; set; }
            public int EscuelaId { get; set; }

            [Required(ErrorMessage = "Ingrese el nombre de la escuela emisora")]
            [Display(Name = "Escuela Emisora:")]
            public string EscuelaEmisora { get; set; }

            [Required(ErrorMessage = "Ingrese un curso")]
            [Display(Name = "Curso:")]
            public string Curso { get; set; }

            [Required(ErrorMessage = "Ingrese una categoría")]
            [Display(Name = "Categoría:")]
            public string Categoria { get; set; }

            [Required(ErrorMessage = "Ingrese el costo total")]
            [Display(Name = "Costo Total:")]
            public decimal CostoTotal { get; set; }

            [Required(ErrorMessage = "Ingrese el promotor")]
            [Display(Name = "Promotor:")]
            public string Promotor { get; set; }

            [Required(ErrorMessage = "Ingrese una fecha de inicio de clases")]
            [Display(Name = "Inicio de Clases:")]
            [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FecIngreso { get; set; }


            [Required(ErrorMessage = "Ingrese la fecha de reporte")]
            [Display(Name = "Fecha Reporte:")]
            [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime FecReporte { get; set; }

            [Required(ErrorMessage = "Ingrese las observaciones aquí")]
            [Display(Name = "Observaciones:")]
            [AllowHtml]
            public string Observacion { get; set; }
            
            

            // para mostrar en la vista 
            public SelectList Escuelas { get; set; }

            public string Alumno { get; set; }
        }   

}
