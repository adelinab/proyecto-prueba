﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Areas.Interna.Models
{
    public class BuscaEscuelasViewModel
    {
        public BuscaEscuelasViewModel()
        {
            Escuelas = new List<EscuelasListaViewModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<EscuelasListaViewModel> Escuelas { get; set; }
    }
}
