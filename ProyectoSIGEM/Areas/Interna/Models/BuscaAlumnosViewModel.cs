﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoSIGEM.Areas.Interna.Models
{
    public class BuscaAlumnosViewModel
    {
        public BuscaAlumnosViewModel()
        {
            Alumnos = new List<AlumnosListaViewModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<AlumnosListaViewModel> Alumnos { get; set; }
    }
}