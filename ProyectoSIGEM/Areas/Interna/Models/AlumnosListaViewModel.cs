﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Areas.Interna.Models
{
    public class AlumnosListaViewModel
    {
        public int AlumnoId { get; set; }
        public string Nombre { get; set; }
        public string DocumentoIdentidad { get; set; }
        public string Telefono { get; set; }
        public string DireccionCalle { get; set; }
    }
}
