﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Interna.Models
{
    public class GrabaEscuelaViewModel
    {
        public int EscuelaId { get; set; }
        [Required]
        [Display(Name="Nombre:")]
        [MaxLength(60)]
        public string Nombre { get; set; }

        public string DireccionDepartamento { get; set; }
        [Required]
        [Display(Name = "Departamento:")]
        public SelectList Departamentos { get; set; }


        public string DireccionProvincia { get; set; }
        [Required]
        [Display(Name = "Provincia:")]
        public SelectList Provincias { get; set; }


        public string DireccionDistrito { get; set; }
        [Required]
        [Display(Name = "Distrito:")]
        public SelectList Distritos { get; set; }


        [Required]
        [Display(Name = "Dirección:")]
        [MaxLength(60)]
        public string DireccionCalle { get; set; }


        [Required]
        [Display(Name = "Teléfono:")]
        [MaxLength(12)]
        public string Telefono { get; set; }

        public GrabaEscuelaViewModel()
        {
            var emptyList = new List<string> { "Seleccione..." };
            Departamentos = new SelectList(emptyList);
            Provincias = new SelectList(emptyList);
            Distritos = new SelectList(emptyList);
        }
    }
}
