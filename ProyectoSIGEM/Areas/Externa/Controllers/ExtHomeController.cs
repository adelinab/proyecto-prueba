﻿using AutoMapper;
using ProyectoSIGEM.Areas.Externa.Models;
using ProyectoSIGEM.Servicios.Alumnos;
using ProyectoSIGEM.Servicios.Matriculas;
using ProyectoSIGEM.Servicios.Matriculas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Externa.Controllers
{
    [Authorize(Roles = "Alumno")]
    public class ExtHomeController : Controller
    {
       private ServicioMatriculas servicioMatriculas;
       private ServicioAlumnos servicioAlumnos;


       public ExtHomeController()
        {
            servicioMatriculas = new ServicioMatriculas();
            servicioAlumnos = new ServicioAlumnos();
        }

        public ActionResult Index(string dni)
        {
            int alumnoid;
            if(string.IsNullOrEmpty(dni)){

            alumnoid = 0;
            }else{
             alumnoid = servicioAlumnos.TraerPorDNI(dni);}

            var model = Mapper.Map<IList<MatriculaDto>, IList<MatriculasListaViewModel>>(servicioMatriculas.BuscarIdAlumnos(alumnoid));
            return View(new BuscaMatriculasViewModel() { Matriculas = model });
        }

        public PartialViewResult ConsultarMatriculasPub(string busqueda)
        {
            var model = Mapper.Map<IList<MatriculaDto>, IList<MatriculasListaViewModel>>(
                servicioMatriculas.Buscar(busqueda));

            return PartialView("_ListaMatric", model);
        }

    }
}