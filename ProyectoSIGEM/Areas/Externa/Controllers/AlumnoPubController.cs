﻿using AutoMapper;
using ProyectoSIGEM.Areas.Interna.Models;
using ProyectoSIGEM.Servicios.Matriculas;
using ProyectoSIGEM.Servicios.Matriculas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Externa.Controllers
{
    [Authorize(Roles = "Empleado, Admin")]
    public class AlumnoPubController : Controller
    {
        private ServicioMatriculas servicioMatriculas;

        public AlumnoPubController()
        {
            servicioMatriculas = new ServicioMatriculas();
        }

        // GET: Externa/Alumno
        public ActionResult Index(string busqueda)
        {
            var model = Mapper.Map<IList<MatriculaDto>, IList<MatriculasListaViewModel>>(servicioMatriculas.Buscar(busqueda));
            return View(new BuscaMatriculasViewModel() { Matriculas = model });
        }

        public PartialViewResult ConsultarMatriculasPub(string busqueda)
        {
            var model = Mapper.Map<IList<MatriculaDto>, IList<MatriculasListaViewModel>>(
                servicioMatriculas.Buscar(busqueda));

            return PartialView("_ListaMatric", model);
        }
    }
}