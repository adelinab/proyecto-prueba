﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Areas.Externa.Models
{
    public class BuscaMatriculasViewModel
    {
        public BuscaMatriculasViewModel()
        {
            Matriculas = new List<MatriculasListaViewModel>();
            
        }
        public int Busqueda { get; set; }
        public IEnumerable<MatriculasListaViewModel> Matriculas { get; set; }
    }
}
