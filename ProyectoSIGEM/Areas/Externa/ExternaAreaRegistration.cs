﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoSIGEM.Areas.Externa
{
    public class ExternaAreaRegistration : AreaRegistration
    {
        public override string AreaName 
        {
            get
            {
                return "Externa";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            context.MapRoute(
                name: "userHome",
                url: "{usuario}",
                defaults: new { controller = "ExtHome", action = "Index" }
                );

            context.MapRoute(
                "Externa_default",
                "Externa/{controller}/{action}/{id}",
                new { action = "Index", controller = "ExtHome", id = UrlParameter.Optional }
            );
        }
    }
}