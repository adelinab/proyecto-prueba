﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProyectoSIGEM.Startup))]
namespace ProyectoSIGEM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
