﻿using ProyectoSIGEM.Areas.Interna.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;


namespace ProyectoSIGEM.Models
{
    public class ViewModelBuilder
    {
        internal MatriculaViewModel MatriculaViewModel(MatriculaGrabarViewModel matricula)
        {
            var matriculaViewModel = new MatriculaViewModel();
            matriculaViewModel.AlumnoId = matricula.AlumnoId.ToString();
            matriculaViewModel.EscuelaId = matricula.EscuelaId;
            matriculaViewModel.EscuelaEmisora = matricula.EscuelaEmisora;
            matriculaViewModel.Categoria = matricula.Categoria;
            matriculaViewModel.Curso = matricula.Curso;
            matriculaViewModel.CostoTotal = matricula.CostoTotal;
            matriculaViewModel.Promotor = matricula.Promotor;
            matriculaViewModel.Observacion = matricula.Observacion;
            matriculaViewModel.FecIngreso = matricula.FecIngreso;
            matriculaViewModel.FecReporte = matricula.FecReporte;

            ConfigurarListas(matriculaViewModel);

            return matriculaViewModel;
        }

        internal MatriculaViewModel MatriculaViewModel()
        {
            var matricula = new MatriculaViewModel();
            ConfigurarListas(matricula);
            return matricula;
        }

        private static void ConfigurarListas(MatriculaViewModel matriculaViewModel)
        {
            //var cursos = new List<String>() { "Nueva Licencia", "Recategorización", "Sensibilización", "Revalidación" };
            //var categorias = new List<String>() { "AIIA", "AIIIA", "AI", "AIIB" , "AIIIC"};
            //matriculaViewModel.Cursos = new SelectList(cursos);
            //matriculaViewModel.Categorias = new SelectList(categorias);
            //var cursos = new List<String>()
            //{
            //    new string {Nombre = "Naranjal"},
            //    new EscuelasViewModel {Id = 2, Nombre = "Lurin"},
            //    new EscuelasViewModel {Id = 3, Nombre = "Los Olivos"}
            //};

            //matriculaViewModel.Escuelas = new SelectList(escuelas, "Id", "Nombre", matriculaViewModel.EscuelaId);
        }
             
    }
}
