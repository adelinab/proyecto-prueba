﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoSIGEM.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Desarrollo de un proyecto implementando lo aprendido en el curso MVC.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Adelina Bravo C.";

            return View();
        }
    }
}