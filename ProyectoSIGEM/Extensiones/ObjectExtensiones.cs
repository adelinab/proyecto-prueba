﻿using System;

namespace System
{
    public static class ObjectExtensiones
    {
        public static bool EsNulo(this object objeto)
        {
            return (objeto == null);
        }

        public static bool CompararFechas(this DateTime fechaingreso, DateTime fechareporte)
        {
            bool valor = true;
            if (fechaingreso > fechareporte) { valor = false; }
            return valor;
        }

    }
}