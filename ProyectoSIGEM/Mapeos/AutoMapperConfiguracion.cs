﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProyectoSIGEM.Servicios.Alumnos.Dtos;
using ProyectoSIGEM.Servicios.Matriculas.Dto;
using ProyectoSIGEM.Areas.Interna.Models;
using ProyectoSIGEM.Servicios.Escuelas.Dtos;

namespace ProyectoSIGEM.Web.Mapeos
{
    public class AutoMapperConfiguracion
    {
        public static void Configuracion()
        {

            Mapper.CreateMap<GrabaAlumnoViewModel, GrabaAlumnoDto>()
                .ForMember(dest => dest.Direccion,
                            map => map.MapFrom(
                                orig => new ProyectoSIGEM.Servicios.Alumnos.Dtos.DireccionDto()
                                {
                                    Departamento = orig.DireccionDepartamento,
                                    Provincia = orig.DireccionProvincia,
                                    Distrito = orig.DireccionDistrito,
                                    Calle = orig.DireccionCalle
                                }
                                )
                                );
            Mapper.CreateMap<AlumnosDto, AlumnosListaViewModel>()
                .ForMember(dest => dest.AlumnoId,
                    map => map.MapFrom(orig => orig.AlumnoId))
                .ForSourceMember(orig => orig.Nombre, map => map.Ignore());

            Mapper.CreateMap<AlumnosDto, GrabaAlumnoViewModel>()
                .ForMember(dest => dest.Departamentos,
                    map => map.Ignore())
                .ForMember(dest => dest.Provincias,
                    map => map.Ignore())
                .ForMember(dest => dest.Distritos,
                    map => map.Ignore());

            Mapper.CreateMap<GrabaAlumnoViewModel, AlumnosDto>()
               .ForMember(dest => dest.Direccion,
                   map =>
                       map.MapFrom(
                           orig =>
                               new ProyectoSIGEM.Servicios.Alumnos.Dtos.DireccionDto()
                               {
                                   Departamento = orig.DireccionDepartamento,
                                   Calle = orig.DireccionCalle,
                                   Provincia = orig.DireccionProvincia,
                                   Distrito = orig.DireccionDistrito
                               }))
                .ForSourceMember(orig => orig.Departamentos,
                   map => map.Ignore())
               .ForSourceMember(orig => orig.Provincias,
                   map => map.Ignore())
               .ForSourceMember(orig => orig.Distritos,
                   map => map.Ignore());

            Mapper.CreateMap<GrabaEscuelaViewModel, GrabaEscuelaDto>()
                .ForMember(deste => deste.Direccion,
                map => map.MapFrom(orig => new ProyectoSIGEM.Servicios.Escuelas.Dtos.DireccionDto()
                {
                    Departamento = orig.DireccionDepartamento,
                    Provincia = orig.DireccionProvincia,
                    Distrito = orig.DireccionDistrito,
                    Calle = orig.DireccionCalle
                }));

            Mapper.CreateMap<EscuelasDto, EscuelasListaViewModel>()
                .ForMember(dest => dest.EscuelaId,
                map => map.MapFrom(orig => orig.EscuelaId));

            Mapper.CreateMap<EscuelasDto, GrabaEscuelaViewModel>()
                .ForMember(dest => dest.Departamentos,
                    map => map.Ignore())
                .ForMember(dest => dest.Provincias,
                    map => map.Ignore())
                .ForMember(dest => dest.Distritos,
                    map => map.Ignore());

            Mapper.CreateMap<GrabaEscuelaViewModel, EscuelasDto>()
               .ForMember(dest => dest.Direccion,
                   map =>
                       map.MapFrom(
                           orig =>
                               new ProyectoSIGEM.Servicios.Escuelas.Dtos.DireccionDto()
                               {
                                   Departamento = orig.DireccionDepartamento,
                                   Calle = orig.DireccionCalle,
                                   Provincia = orig.DireccionProvincia,
                                   Distrito = orig.DireccionDistrito
                               }))
                .ForSourceMember(orig => orig.Departamentos,
                   map => map.Ignore())
               .ForSourceMember(orig => orig.Provincias,
                   map => map.Ignore())
               .ForSourceMember(orig => orig.Distritos,
                   map => map.Ignore());

            Mapper.CreateMap<MatriculaGrabarViewModel, MatriculaDto>();

            Mapper.CreateMap<MatriculaDto, MatriculasListaViewModel>()
                .ForMember(dest => dest.MatriculaId,
                map => map.MapFrom(orig => orig.MatriculaId));

            Mapper.CreateMap<MatriculaDto, ProyectoSIGEM.Areas.Externa.Models.MatriculasListaViewModel>()
               .ForMember(dest => dest.MatriculaId,
               map => map.MapFrom(orig => orig.MatriculaId));

            Mapper.CreateMap<MatriculaDto, MatriculaViewModel>();
            Mapper.CreateMap<MatriculaDto, MatriculaGrabarViewModel>();

        }
    }
}