﻿namespace ProyectoSIGEM.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ProyectoSIGEM.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ProyectoSIGEM.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProyectoSIGEM.Models.ApplicationDbContext context)
        {
            var userManager =
                new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager =
                new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // Crear Roles

            roleManager.Create(new IdentityRole("Admin"));
            roleManager.Create(new IdentityRole("Alumno"));
            roleManager.Create(new IdentityRole("Empleado"));

            // Crear Usuario (admin)
            var user = new ApplicationUser()
            {
                UserName = "admin@mailinator.com",
                Email = "admin@mailinator.com",
                Area = "Soporte",
                Nombre = "SysAdmin"
            };

            userManager.Create(user, "P@$$w0rd");
            userManager.AddToRole(user.Id, "Admin");

            // Crear Usuario (empleado)
            var empleado = new ApplicationUser()
            {
                UserName = "empleado@mailinator.com",
                Email = "empleado@mailinator.com",
                Area = "Ventas",
                Nombre = "Empleado de Prueba"

            };

            userManager.Create(empleado, "P@$$w0rd");
            userManager.AddToRole(empleado.Id, "Empleado");


            var alumno = new ApplicationUser()
            {
                UserName = "alumno@mailinator.com",
                Email = "alumno@mailinator.com",
                Nombre = "Alumno de Prueba"
            };

            userManager.Create(alumno, "P@$$w0rd");
            userManager.AddToRole(alumno.Id, "Alumno");
        }
    }
}
