﻿using ProyectoSIGEM.Repositorios.Dominio;
using ProyectoSIGEM.Repositorios.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Repositorios.Impl
{
    public class RepositorioMatricula : RepositorioBase<Matricula,GemDbContext>
    {
        public RepositorioMatricula(GemDbContext context)
            : base(context)
        { }
    }
}
