﻿using ProyectoSIGEM.Repositorios.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Repositorios.Impl
{
    public interface IRepositorio<TEntidad> where TEntidad : Entidad
    {
        void Actualizar(TEntidad entidad);
        void Agregar(TEntidad entidad);
        void Eliminar(TEntidad entidad);
        IQueryable<TEntidad> Traer();
        TEntidad Traer(int id);
    }
}
