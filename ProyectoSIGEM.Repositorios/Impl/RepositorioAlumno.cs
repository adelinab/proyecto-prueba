﻿using ProyectoSIGEM.Repositorios.Dominio;
using ProyectoSIGEM.Repositorios.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Repositorios.Impl
{
    public class RepositorioAlumno : RepositorioBase<Alumno, GemDbContext>
    {
        public RepositorioAlumno(GemDbContext context)
            : base(context)
        { }
    }
}
