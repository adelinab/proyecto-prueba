namespace ProyectoSIGEM.Repositorios.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alumno",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        DocumentoIdentidad = c.String(),
                        Direccion_Departamento = c.String(maxLength: 50),
                        Direccion_Provincia = c.String(maxLength: 50),
                        Direccion_Distrito = c.String(maxLength: 50),
                        Direccion_Calle = c.String(maxLength: 100),
                        Telefono = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Escuela",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Direccion_Departamento = c.String(maxLength: 50),
                        Direccion_Provincia = c.String(maxLength: 50),
                        Direccion_Distrito = c.String(maxLength: 50),
                        Direccion_Calle = c.String(maxLength: 100),
                        Telefono = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Matricula",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AlumnoId = c.Int(nullable: false),
                        EscuelaId = c.Int(nullable: false),
                        EscuelaEmisora = c.String(),
                        Curso = c.String(),
                        Categoria = c.String(),
                        CostoTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Promotor = c.String(),
                        FecIngreso = c.DateTime(nullable: false),
                        FecReporte = c.DateTime(nullable: false),
                        Observacion = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Alumno", t => t.AlumnoId, cascadeDelete: true)
                .Index(t => t.AlumnoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Matricula", "AlumnoId", "dbo.Alumno");
            DropIndex("dbo.Matricula", new[] { "AlumnoId" });
            DropTable("dbo.Matricula");
            DropTable("dbo.Escuela");
            DropTable("dbo.Alumno");
        }
    }
}
