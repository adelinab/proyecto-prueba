﻿using ProyectoSIGEM.Repositorios.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Repositorios.DDDContext
{
    public class EscuelaRepositorio : GenericRepository<EFProyectoRepository, Escuela>
    {
        public EscuelaRepositorio(EFProyectoRepository context)
            : base(context)
        {
        }
    }
}
