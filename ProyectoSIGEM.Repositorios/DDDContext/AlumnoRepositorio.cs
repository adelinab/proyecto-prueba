﻿using ProyectoSIGEM.Repositorios.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Repositorios.DDDContext
{
    public class AlumnoRepositorio : GenericRepository<EFProyectoRepository, Alumno>
    {
        public AlumnoRepositorio(EFProyectoRepository context)
            : base(context)
        {
        }
    }
}
