﻿using ProyectoSIGEM.Repositorios.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Repositorios.DDDContext
{
    public interface IProyectoRepository : IDisposable
    {
        IGenericRepository<Alumno> AlumnoRepository { get; }
        IGenericRepository<Escuela> EscuelaRepository { get; }
        IGenericRepository<Matricula> MatriculaRepository { get; }
        void Commit();
    }
}
