﻿using ProyectoSIGEM.Repositorios.Dominio;
using ProyectoSIGEM.Repositorios.Dominio.Componentes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Repositorios.DDDContext
{
    public class EFProyectoRepository : DbContext, IProyectoRepository
    {
        private readonly AlumnoRepositorio _alumnorepo;
        private readonly EscuelaRepositorio _escuelarepo;
        private readonly MatriculaRepositorio _matricularepo;

        public DbSet<Alumno> Alumnos { get; set; }
        public DbSet<Escuela> Escuelas { get; set; }
        public DbSet<Matricula> Matriculas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ComplexType<Direccion>().Property(p => p.Departamento).HasMaxLength(50);
            modelBuilder.ComplexType<Direccion>().Property(p => p.Provincia).HasMaxLength(50);
            modelBuilder.ComplexType<Direccion>().Property(p => p.Distrito).HasMaxLength(50);
            modelBuilder.ComplexType<Direccion>().Property(p => p.Calle).HasMaxLength(100);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public EFProyectoRepository()
            : base("name=Gem")
        {
            _alumnorepo = new AlumnoRepositorio(this);
            _escuelarepo = new EscuelaRepositorio(this);
            _matricularepo = new MatriculaRepositorio(this);
        }

        #region IUnitOfWork Implementation



        public void Commit()
        {
            this.SaveChanges();
        }

        #endregion

        public IGenericRepository<Alumno> AlumnoRepository
        {
            get { return _alumnorepo; }
        }

        public IGenericRepository<Escuela> EscuelaRepository
        {
            get { return _escuelarepo; }
        }

        public IGenericRepository<Matricula> MatriculaRepository
        {
            get { return _matricularepo; }
        }

    }
}
