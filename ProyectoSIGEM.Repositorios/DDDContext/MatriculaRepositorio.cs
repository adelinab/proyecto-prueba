﻿using ProyectoSIGEM.Repositorios.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Repositorios.DDDContext
{
    public class MatriculaRepositorio : GenericRepository<EFProyectoRepository, Matricula>
    {
        public MatriculaRepositorio(EFProyectoRepository context)
            : base(context)
        {
        }
    }
}
