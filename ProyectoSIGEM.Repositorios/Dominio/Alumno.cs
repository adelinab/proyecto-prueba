﻿using ProyectoSIGEM.Repositorios.Dominio.Componentes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Repositorios.Dominio
{
    public class Alumno : Entidad
    {
        public string Nombre { get; set; }
        public string DocumentoIdentidad { get; set; }
        public Direccion Direccion { get; set; }
        public string Telefono { get; set; }


        public Alumno()
        {
            Direccion = new Direccion();
        }
    }
}
