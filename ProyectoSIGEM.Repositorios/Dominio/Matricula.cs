﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Repositorios.Dominio
{
    public class Matricula:Entidad
    {

        public int AlumnoId { get; set; }
        public int EscuelaId { get; set; }
        public string EscuelaEmisora { get; set; }
        public string Curso { get; set; }
        public string Categoria { get; set; }
        public decimal CostoTotal { get; set; }
        public string Promotor { get; set; }
        public DateTime FecIngreso { get; set; }
        public DateTime FecReporte { get; set; }
        public string Observacion { get; set; }

        public virtual Alumno Alumno { get; set; }
        
        public Matricula(int alumnoId, int escuelaId, string escuelaEmisora, 
                         string curso, string categoria, decimal costototal, 
                         string promotor,  DateTime fecIngreso, 
                         DateTime fecReporte, string observacion)
        {
            // TODO: Complete member initialization
            this.AlumnoId = alumnoId;
            this.EscuelaId = escuelaId;
            this.EscuelaEmisora = escuelaEmisora;
            this.Curso = curso;
            this.Categoria = categoria;
            this.CostoTotal = costototal;
            this.Promotor = promotor;
            this.FecIngreso = fecIngreso;
            this.FecReporte = fecReporte;
            this.Observacion = observacion;
        }

        public Matricula() { 
        }
        
    }
}
