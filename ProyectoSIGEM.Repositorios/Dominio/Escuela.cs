﻿using ProyectoSIGEM.Repositorios.Dominio.Componentes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Repositorios.Dominio
{
    public class Escuela : Entidad
    {
        public string Nombre { get; set; }
        public Direccion Direccion { get; set; }
        public string Telefono { get; set; }

        public Escuela()
        {
            Direccion = new Direccion();
        }
    }
}
