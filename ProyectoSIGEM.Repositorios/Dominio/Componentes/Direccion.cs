﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoSIGEM.Repositorios.Dominio.Componentes
{
    [ComplexType]
    public class Direccion
    {
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Calle { get; set; }
    }
}
