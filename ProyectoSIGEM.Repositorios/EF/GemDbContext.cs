﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using ProyectoSIGEM.Repositorios.Dominio;
using ProyectoSIGEM.Repositorios.Dominio.Componentes;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ProyectoSIGEM.Repositorios.EF
{
    public class GemDbContext : DbContext
    {
        public GemDbContext()
            : base("name=Gem")
        {

        }
        public GemDbContext(string conexion)
            : base(conexion)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ComplexType<Direccion>().Property(p => p.Departamento).HasMaxLength(50);
            modelBuilder.ComplexType<Direccion>().Property(p => p.Provincia).HasMaxLength(50);
            modelBuilder.ComplexType<Direccion>().Property(p => p.Distrito).HasMaxLength(50);
            modelBuilder.ComplexType<Direccion>().Property(p => p.Calle).HasMaxLength(100);
            
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public virtual DbSet<Alumno> Alumnos { get; set; }
        public virtual DbSet<Escuela> Escuelas { get; set; }
        public virtual DbSet<Matricula> Matriculas { get; set; }
    }
}
