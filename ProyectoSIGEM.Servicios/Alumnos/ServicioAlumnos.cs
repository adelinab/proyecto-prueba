﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoSIGEM.Repositorios.Impl;
using ProyectoSIGEM.Repositorios.EF;
using ProyectoSIGEM.Servicios.Alumnos.Dtos;
using ProyectoSIGEM.Repositorios.Dominio;
using AutoMapper;
using ProyectoSIGEM.Repositorios.DDDContext;
using ProyectoSIGEM.Repositorios.Dominio.Componentes;

namespace ProyectoSIGEM.Servicios.Alumnos
{
    public class ServicioAlumnos
    {
        //private RepositorioAlumno repositorioAlumno;
        private IProyectoRepository _contexto;

        public ServicioAlumnos()
        {
            //repositorioAlumno = new RepositorioAlumno(new GemDbContext("name=Gem"));
            _contexto = new EFProyectoRepository();
        }

        public void Nuevo(GrabaAlumnoDto grabaAlumnoDto)
        {
            var alumno = Mapper.Map<GrabaAlumnoDto, Alumno>(grabaAlumnoDto);
            _contexto.AlumnoRepository.Add(alumno);
            //repositorioAlumno.Agregar(alumno);
            _contexto.Commit();
        }

        public IList<AlumnosDto> Buscar(string busqueda)
        {
            var alumnos = _contexto.AlumnoRepository.GetAll();//repositorioAlumno.Traer();
            if (!string.IsNullOrEmpty(busqueda))
                alumnos = alumnos.Where(c => c.Nombre.Contains(busqueda));

            return Mapper
                .Map<IList<Alumno>, IList<AlumnosDto>>(alumnos.ToList());
        }
        
        public AlumnosDto TraerPorId(int id)
        {
            var alumno = _contexto
                .AlumnoRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Alumno, AlumnosDto>(alumno);
        }

        public int TraerPorDNI(string dni)
        {
            var alumno = _contexto
                .AlumnoRepository
                .SingleOrDefault(x => x.DocumentoIdentidad == dni);
            
            return alumno.Id;
        }

        public void Actualizar(AlumnosDto alumnoDto)
        {
            //Forma 1

            //var cliente = Mapper.Map<ClienteDto, Cliente>(clienteDto);

            ///Forma 2 a mano
            var alumnoOld = _contexto.AlumnoRepository.SingleOrDefault(x => x.Id == alumnoDto.AlumnoId);

            alumnoOld.Nombre = alumnoDto.Nombre;
            alumnoOld.DocumentoIdentidad = alumnoDto.DocumentoIdentidad;
            alumnoOld.Direccion = Mapper
                .Map<DireccionDto, Direccion>(alumnoDto.Direccion);
            alumnoOld.Telefono = alumnoDto.Telefono;


            _contexto.AlumnoRepository.Update(alumnoOld);
            _contexto.Commit();

            //forma2 entidades complejas que tienen listas o colecciones
            //var clienteOld = _contexto.ClienteRepository.SingleOrDefault(x => x.Id == clienteDto.Id);

            // actualizar los campos manual
            // si hay colecciones modificar las colecciones manualmente

        }

        public void Eliminar(int id)
        {
            var alumno = _contexto
                .AlumnoRepository
                .SingleOrDefault(x => x.Id == id);

            _contexto.AlumnoRepository.Delete(alumno);
            _contexto.Commit();
        }
    }
}
