﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Alumnos.Dtos
{
    public class GrabaAlumnoDto
    {
        public string Nombre { get; set; }
        public string DocumentoIdentidad { get; set; }
        public DireccionDto Direccion { get; set; }
        public string Telefono { get; set; }
    }
}
