﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Escuelas.Dtos
{
    public class EscuelasDto
    {
        public int EscuelaId { get; set; }
        public string Nombre { get; set; }
        public DireccionDto Direccion { get; set; }
        public string Telefono { get; set; }
    }
}
