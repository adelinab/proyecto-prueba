﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Escuelas.Dtos
{
    public class GrabaEscuelaDto
    {
        public string Nombre { get; set; }
        public DireccionDto Direccion { get; set; }
        public string Telefono { get; set; }
    }
}
