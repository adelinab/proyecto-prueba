﻿using AutoMapper;
using ProyectoSIGEM.Repositorios.DDDContext;
using ProyectoSIGEM.Repositorios.Dominio;
using ProyectoSIGEM.Repositorios.Dominio.Componentes;
using ProyectoSIGEM.Repositorios.EF;
using ProyectoSIGEM.Repositorios.Impl;
using ProyectoSIGEM.Servicios.Escuelas.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Escuelas
{
    public class ServicioEscuelas
    {
        private RepositorioEscuela repositorioEscuela;

        //public ServicioEscuelas() {
        //    repositorioEscuela = new RepositorioEscuela(new GemDbContext("name=Gem"));
        //}

        //public void Nuevo(GrabaEscuelaDto grabaEscuelaDto)
        //{
        //    var escuela = Mapper.Map<GrabaEscuelaDto, Escuela>(grabaEscuelaDto);

        //    repositorioEscuela.Agregar(escuela);
        //}

        //public IList<EscuelasDto> Buscar(string busqueda)
        //{
        //    var escuelas = repositorioEscuela.Traer();
        //    if (!string.IsNullOrEmpty(busqueda))
        //        escuelas = escuelas.Where(c => c.Nombre.Contains(busqueda));

        //    return Mapper
        //        .Map<IList<Escuela>, IList<EscuelasDto>>(escuelas.ToList());
        //}

        private IProyectoRepository _contexto;

        public ServicioEscuelas()
        {
            //repositorioAlumno = new RepositorioAlumno(new GemDbContext("name=Gem"));
            _contexto = new EFProyectoRepository();
        }

        public void Nuevo(GrabaEscuelaDto grabaEscuelaDto)
        {
            var escuela = Mapper.Map<GrabaEscuelaDto, Escuela>(grabaEscuelaDto);
            _contexto.EscuelaRepository.Add(escuela);
            //repositorioAlumno.Agregar(alumno);
            _contexto.Commit();
        }

        public IList<EscuelasDto> Buscar(string busqueda)
        {
            var escuelas = _contexto.EscuelaRepository.GetAll();//repositorioAlumno.Traer();
            if (!string.IsNullOrEmpty(busqueda))
                escuelas = escuelas.Where(c => c.Nombre.Contains(busqueda));

            return Mapper
                .Map<IList<Escuela>, IList<EscuelasDto>>(escuelas.ToList());
        }
        
        public EscuelasDto TraerPorId(int id)
        {
            var escuela = _contexto
                .EscuelaRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Escuela, EscuelasDto>(escuela);
        }

        public int TraerPorNombre(string nombre)
        {
            var escuela = _contexto
                .EscuelaRepository
                .SingleOrDefault(x => x.Nombre == nombre);
            return escuela.Id;
        }

        public void Actualizar(EscuelasDto escuelaDto)
        {
            var escuelaOld = _contexto.EscuelaRepository.SingleOrDefault(x => x.Id == escuelaDto.EscuelaId);

            escuelaOld.Nombre = escuelaDto.Nombre;
            escuelaOld.Direccion = Mapper
                .Map<DireccionDto, Direccion>(escuelaDto.Direccion);
            escuelaOld.Telefono = escuelaDto.Telefono;


            _contexto.EscuelaRepository.Update(escuelaOld);
            _contexto.Commit();
            
        }

        public void Eliminar(int id)
        {
            var escuela = _contexto
                .EscuelaRepository
                .SingleOrDefault(x => x.Id == id);

            _contexto.EscuelaRepository.Delete(escuela);
            _contexto.Commit();
        }
    }
}
