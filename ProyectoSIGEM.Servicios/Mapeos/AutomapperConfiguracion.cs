﻿using AutoMapper;
using ProyectoSIGEM.Repositorios.Dominio;
using ProyectoSIGEM.Repositorios.Dominio.Componentes;
using ProyectoSIGEM.Servicios.Alumnos.Dtos;
using ProyectoSIGEM.Servicios.Escuelas.Dtos;
using ProyectoSIGEM.Servicios.Matriculas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Mapeos
{
    public class ServiciosAutomapperConfiguracion
    {
        public static void Configuracion()
        {
            Mapper.CreateMap<GrabaAlumnoDto, Alumno>();
            Mapper.CreateMap<Alumno, AlumnosDto>()
                .ForMember(dest => dest.AlumnoId, map => map.MapFrom(orig => orig.Id));
            
            Mapper.CreateMap<GrabaEscuelaDto, Escuela>();
            Mapper.CreateMap<Escuela, EscuelasDto>()
                .ForMember(dest => dest.EscuelaId, map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<Matricula, MatriculaDto>()
                .ForMember(dest => dest.MatriculaId, map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<Direccion, ProyectoSIGEM.Servicios.Escuelas.Dtos.DireccionDto>();
            Mapper.CreateMap<ProyectoSIGEM.Servicios.Escuelas.Dtos.DireccionDto, Direccion>();
            Mapper.CreateMap<Direccion, ProyectoSIGEM.Servicios.Alumnos.Dtos.DireccionDto>();
            Mapper.CreateMap<ProyectoSIGEM.Servicios.Alumnos.Dtos.DireccionDto, Direccion>();
        }
    }
}
