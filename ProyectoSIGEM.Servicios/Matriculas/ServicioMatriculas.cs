﻿using AutoMapper;
using ProyectoSIGEM.Repositorios.DDDContext;
using ProyectoSIGEM.Repositorios.Dominio;
using ProyectoSIGEM.Repositorios.EF;
using ProyectoSIGEM.Repositorios.Impl;
using ProyectoSIGEM.Servicios.Excepciones;
using ProyectoSIGEM.Servicios.Matriculas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Matriculas
{
    public class ServicioMatriculas
    {
        private IProyectoRepository _contexto;

        public ServicioMatriculas() 
        {
            //repositorioMatricula = new RepositorioMatricula(new GemDbContext("name=Gem"));
            _contexto = new EFProyectoRepository();
        }

        public void Nueva(Dto.MatriculaDto matriculaDto)
        {
            try
            {
                //DateTime reservaRegistro = matriculaDto.FecRegistro;
                //DateTime reservaFin = matriculaDto.FecFin;
                //DateTime reservaIni = matriculaDto.FecIni;

                Matricula matricula = new Matricula(
                    matriculaDto.AlumnoId,
                    matriculaDto.EscuelaId,                
                    matriculaDto.EscuelaEmisora,
                    matriculaDto.Curso,
                    matriculaDto.Categoria,
                    matriculaDto.CostoTotal,                    
                    matriculaDto.Promotor,
                    matriculaDto.FecIngreso,
                    matriculaDto.FecReporte,
                    matriculaDto.Observacion
                    );

                //repositorioMatricula.Agregar(matricula);
                _contexto.MatriculaRepository.Add(matricula);
                _contexto.Commit();
            }
            catch (Exception ex)
            {
                throw new ErrorEnMatricula("Hubo un error al procesar la matrícula");
            }
        }

        //public IList<MatriculaDto> Buscar(string busqueda)
        //{
        //    var matriculas = repositorioMatricula.Traer();
        //    if (!string.IsNullOrEmpty(busqueda))
        //        matriculas = matriculas.Where(c => c.Categoria.Contains(busqueda));

        //    return Mapper
        //        .Map<IList<Matricula>, IList<MatriculaDto>>(matriculas.ToList());
        //}

        public IList<MatriculaDto> Buscar(string busqueda)
        {
            var matriculas = _contexto.MatriculaRepository.GetAll();
            if (!busqueda.Equals(string.Empty))
                matriculas = matriculas.Where(c => c.Id.Equals(busqueda));
            
            return Mapper
                .Map<IList<Matricula>, IList<MatriculaDto>>(matriculas.ToList());
        }

        public IList<MatriculaDto> BuscarIdAlumnos(int idalumnos)
        {
            var matriculas = _contexto.MatriculaRepository.GetAll();
                     matriculas = matriculas.Where(c => c.AlumnoId.Equals(idalumnos));

            return Mapper
                .Map<IList<Matricula>, IList<MatriculaDto>>(matriculas.ToList());
        }

        public MatriculaDto TraerPorId(int id)
        {
            var matricula = _contexto
                .MatriculaRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Matricula, MatriculaDto>(matricula);
        }

        public void Actualizar(MatriculaDto matriculaDto)
        {
            var matriculaOld = _contexto.MatriculaRepository.SingleOrDefault(x => x.Id == matriculaDto.MatriculaId);

            matriculaOld.EscuelaEmisora = matriculaDto.EscuelaEmisora;
            matriculaOld.Curso = matriculaDto.Curso;
            matriculaOld.Categoria = matriculaDto.Categoria;
            matriculaOld.CostoTotal = matriculaDto.CostoTotal;
            matriculaDto.Observacion = matriculaDto.Observacion;
            matriculaDto.Promotor = matriculaDto.Observacion;

            _contexto.MatriculaRepository.Update(matriculaOld);
            _contexto.Commit();

        }

        public void Eliminar(int id)
        {
            var matricula = _contexto
                .MatriculaRepository
                .SingleOrDefault(x => x.Id == id);

            _contexto.MatriculaRepository.Delete(matricula);
            _contexto.Commit();
        }
    }
}
