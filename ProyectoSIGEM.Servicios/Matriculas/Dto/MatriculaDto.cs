﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Matriculas.Dto
{
    public class MatriculaDto
    {
        public int MatriculaId { get; set; }
        public int AlumnoId { get; set; }
        public int EscuelaId { get; set; }
        public string EscuelaEmisora { get; set; }
        public string Curso { get; set; }
        public string Categoria { get; set; }
        public decimal CostoTotal { get; set; }
        public string Promotor { get; set; }
        public DateTime FecIngreso { get; set; }
        public DateTime FecReporte { get; set; }
        public string Observacion { get; set; }
    }
}
