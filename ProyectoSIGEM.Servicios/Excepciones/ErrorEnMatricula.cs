﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoSIGEM.Servicios.Excepciones
{
    public class ErrorEnMatricula : Exception
    {
        public ErrorEnMatricula(string mensaje)
            : base(mensaje)
        {

        }
    }
}
