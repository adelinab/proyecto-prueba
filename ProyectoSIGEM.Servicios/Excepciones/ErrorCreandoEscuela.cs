﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Servicios.Excepciones
{
    public class ErrorCreandoEscuela : Exception
    {
        public ErrorCreandoEscuela(string mensaje)
            : base(mensaje)
        {

        }
    }
}
