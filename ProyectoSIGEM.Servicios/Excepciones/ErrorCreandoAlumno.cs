﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProyectoSIGEM.Servicios.Excepciones
{
    public class ErrorCreandoAlumno : Exception
    {
        public ErrorCreandoAlumno(string mensaje)
            : base(mensaje)
        {

        }
    }
}
